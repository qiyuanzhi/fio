/*
 *  Pureflash block device IO engine
 */

#include "../fio.h"
#include "../optgroup.h"
#include "pf_client_api.h"

//#define DEBUG

struct fio_pfs_iou {
	struct io_u *io_u;
	int io_seen;
	int io_complete;
};

struct pfs_data {
	struct PfClientVolume* vol;
	struct io_u **aio_events;
	struct io_u **sort_events;
};

struct pfs_options {
	void *pad;
	char *volume_name;
	char *config_file;
	int  busy_poll;
	int use_tcp;
};

static struct fio_option options[] = {
		{
			.name		= "volume",
			.lname		= "pfs_volume_name",
			.type		= FIO_OPT_STR_STORE,
			.help		= "Pureflash volume name",
			.off1		= offsetof(struct pfs_options, volume_name),
			.category	= FIO_OPT_C_ENGINE,
			.group		= FIO_OPT_G_PFBD,
		},
		{
			.name       = "cfg",
			.lname      = "pfs_config_file",
			.type       = FIO_OPT_STR_STORE,
			.help       = "Pureflash config file path",
			.off1       = offsetof(struct pfs_options, config_file),
			.def        = "/etc/pureflash/pf.conf",
			.category   = FIO_OPT_C_ENGINE,
			.group      = FIO_OPT_G_PFBD,
		},
		{
			.name		= "busy_poll",
			.lname		= "Busy poll",
			.type		= FIO_OPT_BOOL,
			.help		= "Busy poll for completions instead of sleeping",
			.off1		= offsetof(struct pfs_options, busy_poll),
			.def		= "0",
			.category	= FIO_OPT_C_ENGINE,
			.group		= FIO_OPT_G_PFBD,
		},
		{
				.name = NULL,
		},
};

static int _fio_setup_pfs_data(struct thread_data *td,
                               struct pfs_data **pfs_data_ptr)
{
	struct pfs_data *pfs_d;

	if (td->io_ops_data)
		return 0;

	pfs_d = calloc(1, sizeof(struct pfs_data));
	if (!pfs_d)
		goto failed;

	pfs_d->aio_events = calloc(td->o.iodepth, sizeof(struct io_u *));
	if (!pfs_d->aio_events)
		goto failed;

	pfs_d->sort_events = calloc(td->o.iodepth, sizeof(struct io_u *));
	if (!pfs_d->sort_events)
		goto failed;

	*pfs_data_ptr = pfs_d;
	return 0;
failed:
	if (pfs_d)
		free(pfs_d);
	return 1;

}

static int _fio_pfs_connect(struct thread_data *td)
{
	struct pfs_data *pfbd = td->io_ops_data;
	struct pfs_options *o = td->eo;

	pfbd->vol = pf_open_volume(o->volume_name,o->config_file, NULL, S5_LIB_VER);
	if(pfbd->vol == NULL)
	{
		log_err("Failed to open volume:%s!", o->volume_name);
		return -1;
	}
	return 0;
}

static void _fio_pfbd_disconnect(struct pfs_data *pfbd)
{
	if (!pfbd)
		return;

	/* shutdown everything */
	if (pfbd->vol) {
		pf_close_volume(pfbd->vol);
		pfbd->vol = NULL;
	}
}

static void _fio_pfbd_finish_aiocb(void* data, int complete_status)
{
	struct fio_pfs_iou *fri = data;
	struct io_u *io_u = fri->io_u;

	fri->io_complete = 1;

	if (complete_status != 0) {
		io_u->error = EIO;
		io_u->resid = io_u->xfer_buflen;
	} else {
		io_u->error = 0;
	}
}

static struct io_u *fio_pfbd_event(struct thread_data *td, int event)
{
	struct pfs_data *pfbd = td->io_ops_data;

	return pfbd->aio_events[event];
}

static inline int fri_check_complete(struct pfs_data *pfbd, struct io_u *io_u,
                                     unsigned int *events)
{
	struct fio_pfs_iou *fri = io_u->engine_data;

	if (fri->io_complete) {
		fri->io_seen = 1;
		pfbd->aio_events[*events] = io_u;
		(*events)++;

		return 1;
	}

	return 0;
}

static inline int pfbd_io_u_seen(struct io_u *io_u)
{
	struct fio_pfs_iou *fri = io_u->engine_data;
	return fri->io_seen;
}

static int pfbd_io_u_cmp(const void *p1, const void *p2)
{
	const struct io_u **a = (const struct io_u **) p1;
	const struct io_u **b = (const struct io_u **) p2;
	uint64_t at, bt;

	at = utime_since_now(&(*a)->start_time);
	bt = utime_since_now(&(*b)->start_time);

	if (at < bt)
		return -1;
	else if (at == bt)
		return 0;
	else
		return 1;
}

static int pfbd_iter_events(struct thread_data *td, unsigned int *events,
                           unsigned int min_evts, int wait)
{
	struct pfs_data *pfbd = td->io_ops_data;
	unsigned int this_events = 0;
	struct io_u *io_u;
	int i, sidx;

	sidx = 0;
	io_u_qiter(&td->io_u_all, io_u, i) {
		if (!(io_u->flags & IO_U_F_FLIGHT))
			continue;
		if (pfbd_io_u_seen(io_u))
			continue;

		if (fri_check_complete(pfbd, io_u, events))
			this_events++;
		else if (wait)
			pfbd->sort_events[sidx++] = io_u;
	}

	if (!wait || !sidx)
		return this_events;

	/*
 	 * 	Sort events, oldest issue first, then wait on as many as we
 	 *  need in order of age. If we have enough events, stop waiting,
 	 *  and just check if any of the older ones are done.
 	 */
	if (sidx > 1)
		qsort(pfbd->sort_events, sidx, sizeof(struct io_u *), pfbd_io_u_cmp);

	for (i = 0; i < sidx; i++) {
		io_u = pfbd->sort_events[i];

		if (fri_check_complete(pfbd, io_u, events)) {
			this_events++;
			continue;
		}

		/*
 		 * Stop waiting when we have enough, but continue checking
 		 * all pending IOs if they are complete.
 		 */
		if (*events >= min_evts)
			continue;

		if (fri_check_complete(pfbd, io_u, events))
			this_events++;
	}

	return this_events;
}

static int fio_pfbd_getevents(struct thread_data *td, unsigned int min,
                             unsigned int max, const struct timespec *t)
{
	unsigned int this_events, events = 0;
	struct pfs_options *o = td->eo;
	int wait = 0;

	do {
		//if (td->terminate)
		//	return -EINTR;
		this_events = pfbd_iter_events(td, &events, min, wait);

		if (events >= min)
			break;
		if (this_events)
			continue;

		if (!o->busy_poll)
			wait = 1;
		else
			nop;
	} while (1);
	return events;
}

static enum fio_q_status fio_pfbd_queue(struct thread_data *td, struct io_u *io_u)
{
	struct pfs_data *pfbd = td->io_ops_data;
	struct fio_pfs_iou *fri = io_u->engine_data;
	int r = -1;
	unsigned int read_retry_cnt = 0;
	unsigned int write_retry_cnt = 0;

	fio_ro_check(td, io_u);

	fri->io_seen = 0;
	fri->io_complete = 0;

RETRY:
	if (io_u->ddir == DDIR_WRITE) {
		r = pf_io_submit_write(pfbd->vol, io_u->xfer_buf, io_u->xfer_buflen, io_u->offset,
		                  _fio_pfbd_finish_aiocb, fri);
		if (r < 0) {
			if(r == -EAGAIN) {
				if (++write_retry_cnt >= 6)	{
					printf("AIO write  got EAGAIN, sleep %d*1ms.\n", write_retry_cnt);
					write_retry_cnt = 0;
				}

				fprintf(stderr, "AIO write  got EAGAIN, sleep %d*1ms.\n", write_retry_cnt);
				usleep(1000);
				goto RETRY;
			}
			goto failed_comp;
		}

	} else if (io_u->ddir == DDIR_READ) {
		r = pf_io_submit_read(pfbd->vol, io_u->xfer_buf, io_u->xfer_buflen, io_u->offset,
		                 _fio_pfbd_finish_aiocb, fri);

		if (r < 0) {
			if(r == -EAGAIN) {
				if (++read_retry_cnt >= 6)
				{
					printf("AIO read got EAGAIN, sleep %d*1ms.\n", read_retry_cnt);
					read_retry_cnt = 0;
				}
				fprintf(stderr, "AIO read got EAGAIN, sleep %d*1ms.\n", read_retry_cnt);
				usleep(1000);
				goto RETRY;
			}
			goto failed_comp;
		}
	} else {
		dprint(FD_IO, "%s: Warning: unhandled ddir: %d\n", __func__,
		       io_u->ddir);
		goto failed_comp;
	}
	return FIO_Q_QUEUED;
failed_comp:
	printf("cmpare\n");
	printf("failed\n");
	io_u->error = r;
	td_verror(td, io_u->error, "xfer");
	return FIO_Q_COMPLETED;
}

static int fio_pfbd_init(struct thread_data *td)
{
	int r = _fio_pfs_connect(td);
	if (r) {
		log_err("fio_pfs_connect failed, return code: %d .\n", r);
		return 1;
	}
	return 0;
}

static void fio_pfbd_cleanup(struct thread_data *td)
{
	struct pfs_data *pfbd = td->io_ops_data;

	if (pfbd) {
		_fio_pfbd_disconnect(pfbd);
		free(pfbd->aio_events);
		free(pfbd->sort_events);
		free(pfbd);
	}
}

static int fio_pfbd_setup(struct thread_data *td)
{
	struct fio_file *f;
	struct pfs_data *pfbd = NULL;
	struct pfs_options *o = td->eo;
	int r;
	struct PfClientVolumeInfo info;
	/* allocate engine specific structure to deal with libs5bd. */
	r = _fio_setup_pfs_data(td, &pfbd);
	if (r) {
		log_err("fio_setup_s5bd_data failed.\n");
		goto cleanup;
	}
	td->io_ops_data = pfbd;

	td->o.use_thread = 1;


	/* taken from "net" engine. Pretend we deal with files,
	 * even if we do not have any ideas about files.
	 * The size of the S5BD is set instead of a artificial file.
	 */
	if (!td->files_index) {
		add_file(td, td->o.filename ? : "pfbd", 0, 0);
		td->o.nr_files = td->o.nr_files ? : 1;
		td->o.open_files++;
	}
	f = td->files[0];

	r = pf_query_volume_info(o->volume_name, o->config_file, NULL, S5_LIB_VER,  &info);
	if (r != 0)
	{
		log_err("pfbd query volume[%s]  failed!", o->volume_name);
		r = -1;
		goto cleanup;
	}
	f->real_file_size = info.volume_size;
	return 0;

cleanup:
	fio_pfbd_cleanup(td);
	return r;
}

static int fio_pfbd_open(struct thread_data *td, struct fio_file *f)
{
	return 0;
}

static int fio_pfbd_invalidate(struct thread_data *td, struct fio_file *f)
{
	return 0;
}

static void fio_pfbd_io_u_free(struct thread_data *td, struct io_u *io_u)
{
	struct fio_pfs_iou *fri = io_u->engine_data;

	if (fri) {
		io_u->engine_data = NULL;
		free(fri);
	}
}

static int fio_pfbd_io_u_init(struct thread_data *td, struct io_u *io_u)
{
	struct fio_pfs_iou *fri;

	fri = calloc(1, sizeof(*fri));
	fri->io_u = io_u;
	io_u->engine_data = fri;
	return 0;
}
static int fio_pfbd_close(struct thread_data *td, struct fio_file *f)
{
	return 0;
}
static struct ioengine_ops ioengine = {
		.name			= "pfbd",
		.version		= FIO_IOOPS_VERSION,
		.setup			= fio_pfbd_setup,
		.init			= fio_pfbd_init,
		.queue			= fio_pfbd_queue,
		.getevents		= fio_pfbd_getevents,
		.event			= fio_pfbd_event,
		.cleanup		= fio_pfbd_cleanup,
		.open_file		= fio_pfbd_open,
		.close_file     = fio_pfbd_close,
		.invalidate		= fio_pfbd_invalidate,
		.options		= options,
		.io_u_init		= fio_pfbd_io_u_init,
		.io_u_free		= fio_pfbd_io_u_free,
		.option_struct_size	= sizeof(struct pfs_options),
};

static void fio_init fio_pfbd_register(void)
{
	register_ioengine(&ioengine);
}

static void fio_exit fio_pfbd_unregister(void)
{
	unregister_ioengine(&ioengine);
}